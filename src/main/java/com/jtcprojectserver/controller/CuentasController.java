package com.jtcprojectserver.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtcprojectserver.bean.Cuentas;
import com.jtcprojectserver.repository.ContactoRepository;

@Controller
@RequestMapping("/cuentas")
public class CuentasController {
	
	@Autowired
	private ContactoRepository contactoRepository;
	
	@GetMapping
	public ResponseEntity<List<Cuentas>> obtenerCuentas(UriComponentsBuilder uBuilder){
		List<Cuentas> cuentas = contactoRepository.obtenerContactos();
		
		try {
			if(cuentas.size() > 0){
				HttpHeaders header = new HttpHeaders();
				URI uri = uBuilder.path("/cuentas/" ).build().toUri();
				header.setLocation(uri);
				return new ResponseEntity<List<Cuentas>>(cuentas, header, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Cuentas>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}
	
	@GetMapping("/{idCuenta}")
	public Cuentas obtenerCuenta(@PathVariable("idCuenta") String idCuenta){
		Cuentas cuenta = contactoRepository.obtenerCuenta(Integer.valueOf(idCuenta));
		try {
			if(cuenta == null){
				return cuenta;
			}
		} catch (Exception e) {
			
		}
		return null;
	}
	

}

package com.jtcprojectserver.repository;

import java.util.List;

import com.jtcprojectserver.bean.Cuentas;



public interface ContactoRepository {
	
	List<Cuentas> obtenerContactos();
	Cuentas obtenerCuenta(Integer idCuenta);
	//Cuentas agregarContacto(Cuentas contacto);
	//boolean eliminarContacto(Integer id);
	//boolean actualizarContacto(Cuentas contacto, Integer id);
	//Cuentas obtenerContacto(Integer id);

}

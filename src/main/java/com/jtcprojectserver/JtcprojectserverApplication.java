package com.jtcprojectserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JtcprojectserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(JtcprojectserverApplication.class, args);
	}
}

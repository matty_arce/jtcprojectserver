package com.jtcprojectserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jtcprojectserver.bean.Usuarios;
import com.jtcprojectserver.repository.LoginRepository;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private LoginRepository loginRepository;
	
	@PostMapping//("/{user}")//{nrodocumento} @PathParam("user") String id
	public boolean login(@RequestBody Usuarios user){
		Usuarios usuario = loginRepository.obtenerUsuario(user.getNroDocumento());
		
		if(usuario != null){
			if(usuario.getPin().equals(user.getPin()))
				return true;
		}
		else
			return false;
		
		return false;
	}

}

package com.jtcprojectserver.repository;

import java.util.List;

import com.jtcprojectserver.bean.Movimientos;

public interface MovimientosRepository {
	
	List<Movimientos> obtenerMovimientos();
	Movimientos agregarMovimientos(Movimientos movimiento);
	//boolean actualizarMovimientos(Movimientos contactos, Integer id);
	//Movimientos agregarMovimiento();
	Movimientos obtenerMovimiento(Integer idCuenta);
	

}

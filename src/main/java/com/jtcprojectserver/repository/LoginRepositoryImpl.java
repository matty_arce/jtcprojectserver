package com.jtcprojectserver.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jtcprojectserver.bean.Usuarios;

@Repository
public class LoginRepositoryImpl implements LoginRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Usuarios obtenerUsuario(String nroDocumento){
		Usuarios usuario = jdbcTemplate.queryForObject("select * from usuarios b where b.nrodocumento = ?",
				new Object[] {nroDocumento},
				(rs, rowNum) -> {
					Usuarios u = new Usuarios();
					u.setIdUsuario(rs.getInt("idusuario"));
					u.setNroDocumento(rs.getString("nrodocumento"));
					u.setNombre(rs.getString("nombre"));
					u.setPin(rs.getString("pin"));
					return u;
				});
		return usuario;
	}

}

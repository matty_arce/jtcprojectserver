package com.jtcprojectserver.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.jtcprojectserver.bean.Cuentas;



@Repository
public class ContactoRepositoryImpl implements ContactoRepository{ //a diferencia del proy rest implemente abstract

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Cuentas> obtenerContactos() {
		List<Cuentas> contactos = jdbcTemplate.query("select * from cuentas", 
				(rs) -> {
					List<Cuentas> list = new ArrayList<>();
					while(rs.next()){
						Cuentas c = new Cuentas();
						c.setIdCuenta(rs.getInt("idcuenta"));
						c.setNroCuenta(rs.getString("nrocuenta"));
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return contactos;
	}
	
	public List<Cuentas> obtenerContactos2() {
		ResultSetExtractor<List<Cuentas>> resultSetExtracto = new ResultSetExtractor<List<Cuentas>>() {

			@Override
			public List<Cuentas> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Cuentas> lista = new ArrayList<>();
				while (rs.next()) {
					Cuentas item = new Cuentas();
					item.setIdCuenta(rs.getInt("idcuenta"));
					item.setNroCuenta(rs.getString("nrocuenta"));
					item.setTipo(rs.getString("tipo"));
					item.setMoneda(rs.getString("moneda"));
					item.setSaldo(rs.getInt("saldo"));
					lista.add(item);
				}
				return lista;
			}
		};
		List<Cuentas> contactos = jdbcTemplate.query("select * from cuentas", resultSetExtracto);
		return contactos;
	}
	
	/*
	 * private Integer idCuenta;
	private String nroCuenta;
	private String tipo;
	private String moneda;
	private Integer saldo;
	 * */
	
	@Override
	public Cuentas obtenerCuenta(Integer idCuenta){
		Cuentas cuenta = jdbcTemplate.queryForObject("select * from cuentas where idcuenta = ?",//ver el valor de idcuenta por idCuenta
				new Object[] {idCuenta},
				(rs, rowNum) -> {
					Cuentas c = new Cuentas();//cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
					c.setIdCuenta(rs.getInt("idcuenta"));
					c.setNroCuenta(rs.getString("nrocuenta"));
					c.setTipo(rs.getString("tipo"));
					c.setMoneda(rs.getString("moneda"));
					c.setSaldo(rs.getInt("saldo"));
					return c;
				});
		return cuenta;
	}
	
	
/*
	@Override
	public Cuentas agregarContacto(Cuentas contacto) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into contactos values (?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setString(2, contacto.getNombre());
			ps.setString(3,  contacto.getMail());
			ps.setString(4, contacto.getDireccion());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			contacto.setId(keyHolder.getKey().intValue());
		
		return contacto;
		
	}

	@Override
	public boolean eliminarContacto(Integer id) {
		int result = jdbcTemplate.update("delete from contactos where id = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarContacto(Contacto contacto, Integer id) {
		int result = jdbcTemplate.update("update contactos set nombre = ?, mail = ?, direccion = ? where id = ?", 
				contacto.getNombre(), contacto.getMail(), contacto.getDireccion(), id);
		return (result > 0) ? true : false;
	}

	@Override
	public Contacto obtenerContacto(Integer id) {
		Contacto contacto = jdbcTemplate.queryForObject("select * from contactos where id = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Contacto c = new Contacto();
					c.setId(rs.getInt("id"));
					c.setNombre(rs.getString("nombre"));
					c.setMail(rs.getString("mail"));
					c.setDireccion(rs.getString("direccion"));
					return c;
		        });
		
		return contacto;
	}
	*/

}

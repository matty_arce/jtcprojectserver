package com.jtcprojectserver.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.jtcprojectserver.bean.Cuentas;
import com.jtcprojectserver.bean.Movimientos;

@Repository
public class MovimientosRepositoryImpl implements MovimientosRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Movimientos> obtenerMovimientos(){
		List<Movimientos> movimientos = jdbcTemplate.query("select * from movimientos", 
				(rs) -> {
					List<Movimientos> list = new ArrayList<>();
					while(rs.next()){
					Movimientos c = new Movimientos();
					//Cuentas n = new Cuentas();
					c.setIdMovimiento(rs.getInt("idmovimiento"));
					c.setFechaHora(rs.getString("fechahora"));
					c.setIdCuenta(rs.getInt("idcuenta"));
					c.setTipoMovimiento(rs.getString("tipomovimiento"));
					c.setMonto(rs.getInt("monto"));
					list.add(c);
				}
				return list;
			});
		return movimientos;
	}
	
	@Override
	public Movimientos agregarMovimientos(Movimientos movimiento){
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into movimientos value (?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
					
					ps.setNull(1, Types.INTEGER);
					ps.setString(2, movimiento.getFechaHora());
					ps.setInt(3, movimiento.getIdCuenta());
					ps.setString(4, movimiento.getTipoMovimiento());
					ps.setInt(5, movimiento.getMonto());
					
			        return ps;
				}; 
				
				int result = jdbcTemplate.update(psc, keyHolder);
				
				if (result > 0)
					movimiento.setIdMovimiento(keyHolder.getKey().intValue());
				
				return movimiento;
		
	}
	
	@Override
	public Movimientos obtenerMovimiento (Integer idCuenta){
		Movimientos movimiento = jdbcTemplate.queryForObject("select * from movimientos where idCuenta = ?", 
				new Object[] {idCuenta}, 
				(rs, rowNum) -> {
					Movimientos c = new Movimientos();
					c.setIdMovimiento(rs.getInt("idmovimiento"));
					c.setFechaHora(rs.getString("fechahora"));
					c.setIdCuenta(rs.getInt("idcuenta"));
					c.setTipoMovimiento(rs.getString("tipomovimiento"));
					c.setMonto(rs.getInt("monto"));
					return c;
				});
		return movimiento;
	}
	

}

package com.jtcprojectserver.repository;

import com.jtcprojectserver.bean.Usuarios;

public interface LoginRepository {
	
	Usuarios obtenerUsuario(String nroDocumento);

}

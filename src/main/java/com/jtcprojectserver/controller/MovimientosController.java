package com.jtcprojectserver.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtcprojectserver.bean.Movimientos;
import com.jtcprojectserver.repository.MovimientosRepository;

@Controller
@RequestMapping("/movimientos")
public class MovimientosController {
	
	@Autowired
	private MovimientosRepository movimientosRepository;
	
	@GetMapping
	public ResponseEntity<List<Movimientos>> obtenerMovimientos(UriComponentsBuilder uBuilder){
		List<Movimientos> movimientos = movimientosRepository.obtenerMovimientos();
		
		try {
			if(movimientos.size() > 0){
				HttpHeaders header = new HttpHeaders();
				URI uri = uBuilder.path("/movimientos/" ).build().toUri();
				header.setLocation(uri);
				return new ResponseEntity<List<Movimientos>>(movimientos, header, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Movimientos>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}
	
	/*
	@GetMapping("/")
	public List<Movimientos> obtenerMovimientos(){
		List<Movimientos> movimientos = movimientosRepository.obtenerMovimientos();
		
		
		return movimientos;
	}*/
	
	@GetMapping ("/{idCuenta}")
	public Movimientos obtenerMovimiento(@PathVariable("idCuenta") String idCuenta){
		Movimientos movimiento = movimientosRepository.obtenerMovimiento(Integer.valueOf(idCuenta));
		try {
			if (movimiento == null) { 
				return movimiento;
			}
		} catch (Exception e) {	}
		
		return null;
	}

}
